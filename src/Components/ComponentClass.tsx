import { Component } from "react";
import axios from "axios";

export class InputClass extends Component {
    state = { url: "", response: "", error: "" };

    render() {

        return (
            <>
                <label>
                    URL: <input value={this.state.url} onChange={(e) => {
                        this.setState({ response: "" });
                        this.setState({ error: "" });
                        this.setState({ url: e.target.value });
                    }} />
                </label>

                <button onClick={async () => {
                    this.setState({ response: "" });
                    this.setState({ error: "" });

                    await axios.get(this.state.url)
                        .then(response => {
                            this.setState({ response: JSON.stringify(response.data) });
                        })
                        .catch(error => {
                            this.setState({ error: error.message });
                        });


                }} disabled={!this.state.url}>���������</button>
                <div>
                    {this.state.response}
                </div>

                <div>
                    <p style={{ color: 'red' }}>{this.state.error}</p>
                </div>
            </>
        );
    }

}