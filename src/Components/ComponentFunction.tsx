﻿import { useState } from "react";
import axios from "axios";

type State = {
    url: string;
    response: string;
    error?: string;
};

export function InputFunction() {
    const [state, setState] = useState<State>({ url: "", response: "", error: "" });

    return (
        <>
            <label>
                URL: <input value={state.url} onChange={(e) => {
                    setState({ response: "", error: "", url: e.target.value });
                }} />
            </label>

            <button onClick={async () => {

                await axios.get(state.url)
                    .then(response => {
                        setState({ response: JSON.stringify(response.data), url: "" });
                    })
                    .catch(error => {
                        setState({ error: error.message, response: "", url: "" });
                    });

            }} disabled={!state.url}>Отправить</button>

            <div>
                {state.response}
            </div>

            <div>
                <p style={{ color: 'red' }}>{state.error}</p>
            </div>
        </>
    );
}
