import './App.css'
import { InputClass } from './Components/ComponentClass';
import { InputFunction } from "./Components/ComponentFunction";

function App() {

  return (
      <div>
          <h1>Component Class</h1>
          <InputClass />

          <h1>Component Function</h1>
          <InputFunction />
    </div>
  )
}

export default App
